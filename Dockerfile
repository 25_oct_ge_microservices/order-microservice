
# Base image containing OS and JDK 11
FROM openjdk:11 as base 

WORKDIR /app

COPY .mvn/ .mvn

COPY mvnw pom.xml ./

RUN chmod +x ./mvnw

RUN ./mvnw dependency:go-offline

# Run the test cases
COPY src src 

CMD [ "./mvnw", "clean", "compile" ]

CMD [ "./mvnw", "clean", "test" ]

#skip the test cases
RUN ./mvnw clean package -DskipTests 

# extract the contents of jar file
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

# Deployment stage
FROM openjdk:11.0.13-jre-slim-buster as stage 


# create an variable
ARG DEPENDENCY=/app/target/dependency

COPY --from=base ${DEPENDENCY}/BOOT-INF/classes /app
COPY --from=base ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=base ${DEPENDENCY}/META-INF /app/META-INF

EXPOSE 8222

ENTRYPOINT [ "java", "-cp", "app:app/lib/*", "com.classpath.ordermicroservice.OrderMicroserviceApplication" ]
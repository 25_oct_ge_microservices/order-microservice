package com.classpath.ordermicroservice.event;

public enum EventType {
    ORDER_PENDING,

    ORDER_PROCESSED,

    ORDER_FULLFILLED,

    ORDER_CANCELLED
}

package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public final class OrderEvent {

    private final EventType eventType;
    private final Order order;

    public OrderEvent(EventType eventType, Order order){
        this.eventType = eventType;
        this.order = order;
    }
}
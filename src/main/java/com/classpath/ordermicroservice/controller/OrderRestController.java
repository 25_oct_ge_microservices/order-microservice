package com.classpath.ordermicroservice.controller;

import java.util.Set;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    @Operation(method = "fetch orders by id")
    public Order fetchOrderByOrderId(@PathVariable long id){
       return this.orderService.findByOrderId(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody Order order){
        return this.orderService.save(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}

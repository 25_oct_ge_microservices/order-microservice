package com.classpath.ordermicroservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import static org.springframework.http.HttpMethod.*;

@Configuration
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers( "/swagger-ui/**", "/actuator/**", "/h2-console/**", "/v3/api-docs/**", "/login.do/**")
                        .permitAll()
                    .antMatchers(GET, "/api/orders/**")
                        .hasAnyAuthority("ROLE_users", "ROLE_admins", "ROLE_super_admins")
                    .antMatchers(POST, "/api/orders/**")
                        .hasAnyAuthority("ROLE_admins", "ROLE_super_admins")
                    .antMatchers(DELETE, "/api/orders/**")
                        .hasAuthority("ROLE_super_admins")
                    .anyRequest()
                        .authenticated()
                .and()
                    .oauth2ResourceServer()
                .jwt();
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        final JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}
package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthIndictor implements HealthIndicator {

    private final OrderRepository orderRepository;
    @Override
    public Health health() {
      long numberOfOrders = this.orderRepository.count();
      if (numberOfOrders <= 0) {
          log.error("Unable to fetch Orders :: ");
          return Health.down().withDetail("DB-Service", "Unable to fetch Orders").build();
      }
        return Health.up().withDetail("DB-Service", "DB is health").build();
    }
}

@Component
@RequiredArgsConstructor
@Slf4j
class KafkaHealthIndictor implements HealthIndicator {

    @Override
    public Health health() {
       return Health.up().withDetail("Kafka-Service", "Kafka is health").build();
    }
}
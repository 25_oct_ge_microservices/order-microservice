package com.classpath.ordermicroservice.service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.classpath.ordermicroservice.event.EventType;
import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final RestTemplate restTemplate;

    private final KafkaTemplate<Long, OrderEvent> kafkaTemplate;

    @CircuitBreaker(name="inventorymicroservice" , fallbackMethod = "fallback")
    @Retry(name = "inventoryRetries", fallbackMethod = "fallback")
    public Order save(Order order){
      log.info("About the make the REST Call to Inventory microservice :: ");
     // final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://inventory-service/api/v1/inventory", null, Integer.class);
      //log.info(" Response from Inventory microservice , {} ", responseEntity.getStatusCode());
      Order savedOrder = this.orderRepository.save(order);
      //push the message to the topic by generating the event
    //  this.kafkaTemplate
      //        .send("orders-topic-new", order.getOrderId(), new OrderEvent(EventType.ORDER_PROCESSED, order));
      return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while updating the inventory");
        log.error(exception.getMessage());
        return Order.builder().orderId(1111).date(LocalDate.now()).customerName("vinod").price(25000).build();
    }

    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order findByOrderId(long orderId){
        return this.orderRepository
                            .findById(orderId)
                            .orElseThrow(() -> new IllegalArgumentException(" Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteAll();
    }
    
}

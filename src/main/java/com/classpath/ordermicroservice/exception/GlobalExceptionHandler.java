package com.classpath.ordermicroservice.exception;

import javax.validation.constraints.Null;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RestControllerAdvice
@Configuration
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Error> handleInvalidOrderId(IllegalArgumentException exception){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(100, exception.getMessage()));
    }  
   
    @ExceptionHandler
    public ResponseEntity<Error> invalidData(NullPointerException exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error(100, exception.getMessage()));
    }
}

@Data
@RequiredArgsConstructor
class Error {
    private final int code;
    private final String message;
}

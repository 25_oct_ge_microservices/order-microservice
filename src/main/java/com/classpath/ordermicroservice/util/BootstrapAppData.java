package com.classpath.ordermicroservice.util;

import static java.util.stream.IntStream.range;

import java.time.LocalDate;

import com.classpath.ordermicroservice.controller.OrderRestController;
import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class BootstrapAppData  implements ApplicationListener<ApplicationReadyEvent>{
 
    private final OrderRepository orderRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event){
        range(1, 10)
            .forEach(index ->{
                Order order = Order.builder()
                                        .customerName("ravi"+index)
                                        .date(LocalDate.now())
                                        .price(25000)
                                .build();
                range(1, 3)
                    .forEach(var ->{
                        order.addLineItem(LineItem.builder().name("random name").price(2200).qty(2).build());
                });                              
                this.orderRepository.save(order);      
        });
    }
}
